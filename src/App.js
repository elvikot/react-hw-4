import "./App.scss";
import { Routes, Route } from "react-router-dom";
import { CartPage, HomePage, ErrorPage, FavorPage } from "./pages";
import Navbar from "./components/Navbar/Navbar";

export default function App(props) {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/cart" element={<CartPage />} />
        <Route path="/favor" element={<FavorPage />} />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </>
  );
}
