import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./FavorIcon.scss";
import PropTypes from "prop-types";

export default function FavorIcon(props) {
  const { productsInFavorAmount } = props;

  return (
    <div className="favor-wrapper">
      <p className="counter">{productsInFavorAmount}</p>
      <FontAwesomeIcon icon={faStar} className="test" />
    </div>
  );
}

FavorIcon.defaultProps = {
  productsInFavorAmount: 0,
};

FavorIcon.propTypes = {
  productsInFavorAmount: PropTypes.number.isRequired,
};
