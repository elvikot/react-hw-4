import { NavLink } from "react-router-dom";
import "./Navbar.scss";
import FavorIcon from "../FavorIcon/FavorIcon";
import CartIcon from "../CartIcon/CartIcon";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import {
  fetchProductsInCart,
  fetchProductsInFavor,
} from "../../redux/actions/products";

export default function Navbar(props) {
  const cart = useSelector((state) => state.products.cart);
  const favor = useSelector((state) => state.products.favor);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProductsInCart());
    dispatch(fetchProductsInFavor());
  }, [dispatch]);

  return (
    <nav className="navbar">
      <NavLink className="navbar__link" to="/">
        Home
      </NavLink>
      <NavLink className="navbar__link" to="/cart">
        Cart
      </NavLink>
      <NavLink className="navbar__link" to="/favor">
        Favor
      </NavLink>
      <div className="inner-wrapper">
        <FavorIcon productsInFavorAmount={Number(favor ? favor.length : 0)} />
        <CartIcon productInCartAmount={Number(cart ? cart.length : 0)} />
      </div>
    </nav>
  );
}
