import "./ProductsList.scss";
import { useEffect, useState } from "react";
import Product from "../Product/Product";
import Modal from "../Modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import {
  addProductToCart,
  addProductToFavor,
  removeProductFromCart,
  removeProductFromFavor,
  selectProduct,
} from "../../redux/actions/products";
import {
  setModalWindowContent,
  setModalWindowStatus,
} from "../../redux/actions/modal";

export default function ProductsList({ productsArr }) {
  const cart = useSelector((state) => state.products.cart);
  const favor = useSelector((state) => state.products.favor);
  const modalWindow = useSelector((state) => state.modalWindow);
  const selectedProduct = useSelector(
    (state) => state.products.selectedProduct
  );
  const dispatch = useDispatch();

  const closeModal = (e) => {
    e.preventDefault();
    if (e.target === e.currentTarget) {
      dispatch(setModalWindowStatus(false));
      dispatch(setModalWindowContent("", ""));
      document.body.style.overflow = "unset";
    }
  };

  const openModal = (modalTitle, modalDescription, action) => {
    dispatch(setModalWindowStatus(true));
    // action === "addingProduct"
    //   ? setModalContent({
    //       title: "Confirm adding",
    //       description: "Do you want to add a product to your cart?",
    //     })
    //   : setModalContent({
    //       title: "Confirm deleting",
    //       description: "Do you want to delete a product from your cart?",
    //     });
    dispatch(setModalWindowContent(modalTitle, modalDescription, action));
    document.body.style.overflow = "hidden";
  };

  const addToCart = () => {
    const modalWindowContent = {
      title: "Confirm adding",
      description: "Do you want to add a product to your cart?",
      action: (selectedProduct) => {
        dispatch(addProductToCart(selectedProduct));
        dispatch(setModalWindowStatus(false));
        dispatch(selectProduct(null));
      },
    };
    openModal(
      modalWindowContent.title,
      modalWindowContent.description,
      modalWindowContent.action
    );
  };

  const removeFromCart = () => {
    const modalWindowContent = {
      title: "Confirm deleting",
      description: "Do you want to delete a product from your cart?",
      action: (selectedProduct) => {
        dispatch(removeProductFromCart(selectedProduct));
        dispatch(setModalWindowStatus(false));
        dispatch(selectProduct(null));
      },
    };
    openModal(
      modalWindowContent.title,
      modalWindowContent.description,
      modalWindowContent.action
    );
  };

  const addToFavor = (product) => {
    dispatch(addProductToFavor(product));
  };

  const removeFromFavor = (product) => {
    dispatch(removeProductFromFavor(product));
  };

  return (
    <>
      <div className="products-list">
        {productsArr.map((product, id) => (
          <Product
            inCart={
              cart.findIndex((obj) => obj.vendorCode === product.vendorCode) !==
              -1
            }
            inFavor={
              favor.findIndex(
                (obj) => obj.vendorCode === product.vendorCode
              ) !== -1
            }
            addToCart={addToCart}
            removeFromCart={removeFromCart}
            addToFavor={addToFavor}
            removeFromFavor={removeFromFavor}
            key={id}
            title={product.title}
            url={product.url}
            vendorCode={product.vendorCode}
            color={product.color}
            price={Number(product.price)}
          />
        ))}
      </div>
      {modalWindow.isOpen ? (
        <Modal
          title={modalWindow.content.title}
          content={modalWindow.content.description}
          action={modalWindow.content.action}
          closeModal={closeModal}
          btns={["Confirm", "Cancel"]}
        />
      ) : null}
    </>
  );
}
