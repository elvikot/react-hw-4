import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./CartIcon.scss";
import PropTypes from "prop-types";

export default function CartIcon(props) {
  const { productInCartAmount } = props;
  return (
    <div className="wrapper">
      <p className="counter">{productInCartAmount}</p>
      <FontAwesomeIcon icon={faShoppingCart} />
    </div>
  );
}
CartIcon.defaultProps = {
  productInCartAmount: 0,
};

CartIcon.propTypes = {
  productInCartAmount: PropTypes.number.isRequired,
};
