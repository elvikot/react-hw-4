import Button from "../Button/Button";
import "./Modal.scss";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

export default function Modal(props) {
  const { title, content, btns } = props;
  const selectedProduct = useSelector(
    (state) => state.products.selectedProduct
  );
  const onClick = () => {
    props.action(selectedProduct);
  };
  return (
    <div className="portal" onClick={props.closeModal}>
      <div className="portal-content">
        <h3 className="title">{title}</h3>
        <p>{content}</p>
        <div className="btn-wrapper">
          <Button className="btn" onClick={onClick} text={btns[0]} />
          <Button className="btn" onClick={props.closeModal} text={btns[1]} />
        </div>
        <span className="close-btn" onClick={props.closeModal}>
          &#11198;
        </span>
      </div>
    </div>
  );
}

Modal.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  btns: PropTypes.array.isRequired,
  action: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
};
