import "./Product.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useDispatch, useSelector } from "react-redux";
import { addProductToFavor, selectProduct } from "../../redux/actions/products";

export default function Product(props) {
  const dispatch = useDispatch();
  const selectedProduct = useSelector(
    (state) => state.products.selectedProduct
  );

  const {
    title,
    url,
    vendorCode,
    price,
    color,
    inCart = false,
    inFavor = false,
  } = props;

  const data = {
    title,
    url,
    vendorCode,
    price,
    color,
  };

  const addToCart = () => {
    dispatch(selectProduct(data));
    props.addToCart();
  };

  const removeFromCart = () => {
    dispatch(selectProduct(data));
    setTimeout(() => {
      props.removeFromCart();
    }, 1000);
  };

  const addToFavor = () => {
    props.addToFavor(data);
  };

  const removeFromFavor = () => {
    props.removeFromFavor(data);
  };

  return (
    <div className="product-item">
      {inFavor ? (
        <span className="star active" onClick={removeFromFavor}></span>
      ) : (
        <span className="star" onClick={addToFavor}></span>
      )}
      <div className="product-item__img-wrapper">
        <img alt="" className="product-item__img" src={url} />
      </div>
      <div className="product-item__info">
        <p className="product-item__title">{title}</p>
        <p className="product-item__vendor-code">Vendor code: {vendorCode}</p>
        <p className="product-item__color">Color: {color}</p>
        <p className="product-item__price">₴{price}</p>
        {inCart ? (
          <Button
            className="product-item__cart added"
            text="x"
            onClick={removeFromCart}
          />
        ) : (
          <Button
            className="product-item__cart"
            text="Add to cart"
            onClick={addToCart}
          />
        )}
      </div>
    </div>
  );
}

Product.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  vendorCode: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
};
