import ProductsList from "../components/ProductsList/ProductsList";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProductsInFavor } from "../redux/actions/products";

export function FavorPage(props) {
  const favor = useSelector((state) => state.products.favor);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProductsInFavor());
  }, [dispatch]);

  return <ProductsList productsArr={favor} />;
}
