import ProductsList from "../components/ProductsList/ProductsList";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchAllProducts } from "../redux/actions/products";

export function HomePage({ actions }) {
  const products = useSelector((state) => state.products.allProducts);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllProducts());
  }, [dispatch]);

  return <ProductsList productsArr={products} actions={actions} />;
}
