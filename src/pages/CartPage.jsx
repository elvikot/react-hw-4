import ProductsList from "../components/ProductsList/ProductsList";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProductsInCart } from "../redux/actions/products";

export function CartPage(props) {
  const cart = useSelector((state) => state.products.cart);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProductsInCart());
  }, [dispatch]);

  return <ProductsList productsArr={cart} />;
}
