// Core
import { combineReducers } from "redux";
import { productsReducer as products } from "./reducers/products";
import { modalWindowReducer as modalWindow } from "./reducers/modal";

// Reducers

export const rootReducer = combineReducers({
  products,
  modalWindow,
});
