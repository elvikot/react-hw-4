import { productsActionTypes as productsTypes } from "../types";

const initialState = {
  allProducts: [],
  cart: [],
  favor: [],
  selectedProduct: null,
};

export function productsReducer(state = initialState, action) {
  switch (action.type) {
    case productsTypes.getAllProducts:
      return {
        ...state,
        allProducts: action.payload,
      };

    case productsTypes.getProductsInCart:
      return {
        ...state,
        cart: action.payload,
      };

    case productsTypes.getProductsInFavor:
      return {
        ...state,
        favor: action.payload,
      };

    case productsTypes.addProductToCart:
      const newCart = [...state.cart, action.payload];
      localStorage.setItem("productsInCart", JSON.stringify(newCart));
      return {
        ...state,
        cart: newCart,
      };

    case productsTypes.removeProductFromCart:
      const productIdxToDelete = state.cart.findIndex(
        (product) => product.vendorCode === action.payload.vendorCode
      );
      const updatedCart = [
        ...state.cart.slice(0, productIdxToDelete),
        ...state.cart.slice(productIdxToDelete + 1),
      ];
      localStorage.setItem("productsInCart", JSON.stringify(updatedCart));
      return {
        ...state,
        cart: updatedCart,
      };

    case productsTypes.addProductToFavor:
      const newFavor = [...state.favor, action.payload];
      localStorage.setItem("productsInFavor", JSON.stringify(newFavor));
      return {
        ...state,
        favor: newFavor,
      };

    case productsTypes.removeProductFromFavor:
      const productIdxToUnfavor = state.favor.findIndex(
        (product) => product.vendorCode === action.payload.vendorCode
      );
      const updatedFavor = [
        ...state.favor.slice(0, productIdxToUnfavor),
        ...state.favor.slice(productIdxToUnfavor + 1),
      ];
      localStorage.setItem("productsInFavor", JSON.stringify(updatedFavor));
      return {
        ...state,
        favor: updatedFavor,
      };

    case productsTypes.selectProduct:
      return {
        ...state,
        selectedProduct: action.payload,
      };
    default:
      return state;
  }
}
