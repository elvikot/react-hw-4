import { modalWindowTypes as modalWindow } from "../types";

const initialState = {
  isOpen: false,
  content: {
    title: "",
    description: "",
    action: "",
  },
};

export function modalWindowReducer(state = initialState, action) {
  switch (action.type) {
    case modalWindow.setStatus:
      return {
        ...state,
        isOpen: action.payload,
      };
    case modalWindow.setContent:
      return {
        ...state,
        content: action.payload,
      };
    default:
      return state;
  }
}
