export const productsActionTypes = {
  getAllProducts: "GET_ALL_PRODUCTS",
  getProductsInCart: "GET_PRODUCTS_IN_CART",
  getProductsInFavor: "GET_PRODUCTS_IN_FAVOR",
  addProductToCart: "ADD_PRODUCT_TO_CART",
  addProductToFavor: "ADD_PRODUCT_TO_FAVOR",
  removeProductFromCart: "REMOVE_PRODUCT_FROM_CART",
  removeProductFromFavor: "REMOVE_PRODUCT_FROM_FAVOR",
  selectProduct: "SELECT_PRODUCT",
};

export const modalWindowTypes = {
  setStatus: "SET_STATUS",
  setContent: "SET_CONTENT",
};
