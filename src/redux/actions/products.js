import { productsActionTypes as productsTypes } from "../types";

export function fillProducts(products) {
  return {
    type: productsTypes.getAllProducts,
    payload: products ? products : [],
  };
}

export const fetchAllProducts = () => async (dispatch) => {
  try {
    const results = await fetch("products.json")
      .then((res) => res.json())
      .then((data) => data);
    dispatch(fillProducts(results));
  } catch (err) {
    console.log(err);
  }
};

export const fetchProductsInCart = () => async (dispatch) => {
  try {
    const productsInCart = JSON.parse(localStorage.getItem("productsInCart"));
    if (!productsInCart) {
      localStorage.setItem("productsInCart", JSON.stringify([]));
    }
    dispatch(getProductsInCart(productsInCart));
  } catch (err) {
    console.log(err);
  }
};

export const fetchProductsInFavor = () => async (dispatch) => {
  try {
    const productsInFavor = JSON.parse(localStorage.getItem("productsInFavor"));
    if (!productsInFavor) {
      localStorage.setItem("productsInFavor", JSON.stringify([]));
    }
    dispatch(getProductsInFavor(productsInFavor));
  } catch (err) {
    console.log(err);
  }
};

export function getProductsInCart(products) {
  return {
    type: productsTypes.getProductsInCart,
    payload: products ? products : [],
  };
}

export function getProductsInFavor(products) {
  return {
    type: productsTypes.getProductsInFavor,
    payload: products ? products : [],
  };
}

export function addProductToCart(product) {
  return {
    type: productsTypes.addProductToCart,
    payload: product,
  };
}

export function removeProductFromCart(product) {
  return {
    type: productsTypes.removeProductFromCart,
    payload: product,
  };
}

export function addProductToFavor(product) {
  return {
    type: productsTypes.addProductToFavor,
    payload: product,
  };
}

export function removeProductFromFavor(product) {
  return {
    type: productsTypes.removeProductFromFavor,
    payload: product,
  };
}

export function selectProduct(product) {
  return {
    type: productsTypes.selectProduct,
    payload: product,
  };
}
