import { modalWindowTypes } from "../types";

export function setModalWindowStatus(status) {
  return {
    type: modalWindowTypes.setStatus,
    payload: status,
  };
}

export function setModalWindowContent(title, description, action) {
  return {
    type: modalWindowTypes.setContent,
    payload: {
      title,
      description,
      action,
    },
  };
}
